
How to run:

1. Unpack the zip file. As a result your should have Finance_CS_Instruments with the project.
1. Run command line
2. Go to Finance_CS_Instruments directory 
3. Build the project. Execute command: mvn clean install
4. Run application:
   a) For Windows OS - Execute the command file: run.bat
   b) For Linux OS - Execute the full command: mvn exec:java -Dexec.mainClass="olek.projects.finance.cs.InstrumentsMain" -Djava.util.logging.config.file=conf/logging.properties
     (I didn't have a chance to test it on Linux)
Note: When running application your current directory should be Finance_CS_Instruments

Prerequisites:
you should have installed and configured on your machine
  - Java version 1.7 or higher
  - maven3

I used following software during development and testing:
  - Java: jdk1.7.0_80
  - maven: apache-maven-3.3.9 or apache-maven-3.0.4
  - IDE: IntelliJ IDEA Community Edition 15.0.1
  - OS: Windows8.1 or Windows7

How to change the input file:
  Substitute the file Finance_CS_Instruments\data\input.csv with your input file. Remember that file name should stay input.csv
  For simplicity reason the input is always taken from that file.
  Later it could be changed to take filename with input data from the command argument.
  
Output results:
  The results calculations of the application appear on console output in the end of execution.
  E.g.:
  Mar 25, 2016 1:17:04 PM olek.projects.finance.cs.calc.DataCalculator calculateResults
  INFO: INSTRUMENT3 result = 218.7183481376518
  Mar 25, 2016 1:17:04 PM olek.projects.finance.cs.calc.DataCalculator calculateResults
  INFO: INSTRUMENT2 result = 6.711729816038750
  Mar 25, 2016 1:17:04 PM olek.projects.finance.cs.calc.DataCalculator calculateResults
  INFO: INSTRUMENT1 result = 3.788540698376163
   
To change logging level or output format:
  Please edit the conf/logging.properties file
   
Decisions:
1) DB Cache.
  I decided to use a cache for MULTIPLIER values because getting value from DB was the most expensive part of the application.
  My measures told me that update of one row in my case would be not less than several milliseconds.
  That's why I decided to take the expiration as 5 milliseconds. It could be changed after thurther PET tests.
  
  Later (I didn't have time) the cache can be improved to remember the particular multiplier change date so if next change is done in less than 5 seconds 
  then we can put the expiration for the left time until we reach 5 seconds.

2) For profiling I used JProfiler. I also created big input files and measured the total time.

3) Threads.
   I decided to make lines processing (parsing and calculations) in chunks in a separate threads.
   This improved time execution, runs at least 20% faster.
   My PET tests showed that 4 threads are enough, increasing threads number doesn't have an impact on the total time execution on my environment.
   Chunk size and threads number can be changed.
   To control the number of threads I used Semaphore.
   
How is it tested:
  1) JUnits. For most critical code I've made JUnits. There was not enough time to cover all code.
  2) Component test. I've calculated in Excel values of input file and later compared it with results produced by my application.
  In the future automated component test could be done - application write results to file and we compare this file with the golden template for our input.
  
  
