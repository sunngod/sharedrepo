package olek.projects.finance.cs;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.opencsv.*;

/**
 * CSV Reader
 */
class InstrumentsFileReader {
    private final static Logger LOGGER = Logger.getLogger(InstrumentsFileReader.class.getName());
    private final static String INPUT_FILE_PATH = "data\\input.csv";
    private final static int LINES_CHUNK_SIZE = 1000;

    private LinesProcessor linesProcessor;

    InstrumentsFileReader(LinesProcessor linesProcessor) {
        this.linesProcessor = linesProcessor;
    }

    public void read() throws IOException {
        LOGGER.info("Start reading input data");

        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(INPUT_FILE_PATH));
            String[] nextLine;
            List<String[]> nextLines = new ArrayList<>(LINES_CHUNK_SIZE);

            while ((nextLine = reader.readNext()) != null) {
                if(LOGGER.isLoggable(Level.FINEST)) {
                    LOGGER.finest("Original line " + nextLine[0] + " \t " + nextLine[1] + " \t " + nextLine[2]);
                }

                if(nextLines.size() == LINES_CHUNK_SIZE) {
                    linesProcessor.processNextLines(nextLines);
                    nextLines = new ArrayList<>(LINES_CHUNK_SIZE);
                }
                nextLines.add(nextLine);
            }
            linesProcessor.processNextLines(nextLines);
            linesProcessor.waitForProcessingIsDone();

            LOGGER.info("Input data reading completed");
        } finally {
            if(reader != null) {
                reader.close();
            }
        }
    }


}
