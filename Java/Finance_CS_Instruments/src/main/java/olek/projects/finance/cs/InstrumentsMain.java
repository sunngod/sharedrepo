package olek.projects.finance.cs;

import olek.projects.finance.cs.calc.DataCalculator;
import olek.projects.finance.cs.db.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class
 */
public class InstrumentsMain {
    private final static Logger LOGGER = Logger.getLogger(InstrumentsMain.class.getName());

    public static void main( String[] args ) throws SQLException {
        DbConnection dbConn = null;
        MultiplierFactorGetter multiplGetter = null;

        try {
            dbConn = DbUtils.getDbConnection();
            (new DbCreator(dbConn.getConnection())).create();
            multiplGetter = new MultiplierFactorGetter(dbConn.getConnection());

            DataCalculator dataCalculator = createDataCalculator();
            LinesProcessor linesProcessor = new LinesProcessor(multiplGetter, dataCalculator);
            InstrumentsFileReader reader = new InstrumentsFileReader(linesProcessor);

            reader.read();
            dataCalculator.calculateResults();

        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Problems with reading input file", ex);
        } finally {
            if(multiplGetter != null) {
                multiplGetter.closeStatement();
            }
            if(dbConn != null) {
                dbConn.close();
            }
            DbUtils.shutdownDB();
        }
    }

    private static DataCalculator createDataCalculator() {
        DataCalculator dataCalculator = new DataCalculator();
        dataCalculator.init();
        return dataCalculator;
    }
}
