package olek.projects.finance.cs;

import olek.projects.finance.cs.calc.DataCalculator;
import olek.projects.finance.cs.calc.InstrumentData;
import olek.projects.finance.cs.calc.LineParser;
import olek.projects.finance.cs.calc.Multiplier;
import olek.projects.finance.cs.db.MultiplierFactorGetter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

class LinesProcessor {
    private final static Logger LOGGER = Logger.getLogger(LinesProcessor.class.getName());
    private final static int MAX_THREADS_NUMBER = 4;

    private Multiplier multiplier;
    private DataCalculator dataCalculator;
    private Semaphore calcThreadsSemaphore = new Semaphore(MAX_THREADS_NUMBER);
    private List<Thread> calcThreads = new ArrayList<>();

    LinesProcessor(MultiplierFactorGetter multiplGetter, DataCalculator dataCalculator) {
        multiplier = new Multiplier(multiplGetter);
        this.dataCalculator = dataCalculator;
    }

    public void processNextLines(final List<String[]> nextLines) {
        try {
            calcThreadsSemaphore.acquire();
            LOGGER.fine("new thread will acquired semaphore");
        } catch (InterruptedException ex) {
            LOGGER.log(Level.SEVERE, "Problems with aquire semaphore", ex);
        }

        final LineParser lineParser = new LineParser();
        Runnable runnableCalc = new Runnable() {
            public void run() {
                LOGGER.fine("starting thread" + Thread.currentThread().getName());
                for(String[] nextLine : nextLines) {
                    processNextLine(nextLine, lineParser);
                }

                calcThreadsSemaphore.release();
                LOGGER.fine("thread finished " + Thread.currentThread().getName());
            }
        };
        Thread calcThread = new Thread(runnableCalc);
        calcThread.start();

        calcThreads.add(calcThread);
    }

    private void processNextLine(String[] nextLine, LineParser lineParser) {
        InstrumentData oneLineData = lineParser.parse(nextLine);
        if(oneLineData != null) {
            multiplier.multiply(oneLineData);
            dataCalculator.add(oneLineData);
        }
    }

    public void waitForProcessingIsDone() {
        joinCalcThreads(calcThreads);
    }

    private void joinCalcThreads(List<Thread> calcThreads) {
        for(Thread calcThread : calcThreads) {
            try {
                calcThread.join();
            } catch (InterruptedException ex) {
                LOGGER.log(Level.SEVERE, "Problems with join calcThread", ex);
            }
        }
    }
}
