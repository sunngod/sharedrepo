package olek.projects.finance.cs.calc;

import java.math.BigDecimal;

class CalculatorUtil {
    static BigDecimal calculateMean(BigDecimal sum, int number) {
        return sum.divide(new BigDecimal(number), InstrumentData.MATH_CONTEXT);
    }
}
