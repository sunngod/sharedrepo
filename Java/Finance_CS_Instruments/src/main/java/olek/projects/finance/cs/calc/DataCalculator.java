package olek.projects.finance.cs.calc;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Processes input data received from reader
 */
public class DataCalculator {
    private final static Logger LOGGER = Logger.getLogger(DataCalculator.class.getName());

    private InstrumentCalculatorFactory calculatorFactory;
    private Map<String, InstrumentCalculator> instrumentCalculators = new HashMap<String, InstrumentCalculator>();

    public void init() {
        calculatorFactory = getCalculatorFactory();
    }

    InstrumentCalculatorFactory getCalculatorFactory() {
        return new InstrumentCalculatorFactory();
    }

    public void add(InstrumentData oneLineData) {
        if (LOGGER.isLoggable(Level.FINEST)) {
            LOGGER.finest(oneLineData.getValue().toString());
        }

        InstrumentCalculator instr = getInstrumentCalculator(oneLineData);
        instr.add(oneLineData);
    }

    private synchronized InstrumentCalculator getInstrumentCalculator(InstrumentData oneLineData) {
        InstrumentCalculator instr = instrumentCalculators.get(oneLineData.getName());
        if(instr == null) {
            instr = calculatorFactory.create(oneLineData.getName());
            instrumentCalculators.put(oneLineData.getName(), instr);
        }
        return instr;
    }

    public void calculateResults() {
        for(InstrumentCalculator instrCalc : instrumentCalculators.values()) {
            LOGGER.info(instrCalc.getName() + " result = " + instrCalc.calculateStatistics());
        }
    }
}