package olek.projects.finance.cs.calc;

import java.math.BigDecimal;

/**
 * INSTRUMENT1
 * Calculates mean for all
 */
public class Instrument1Calculator implements InstrumentCalculator {
    public static final String NAME = "INSTRUMENT1";

    private BigDecimal sum = BigDecimal.ZERO;
    private int number;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public synchronized void add(InstrumentData instrData) {
        sum = sum.add(instrData.getValue());
        number++;
    }

    @Override
    public BigDecimal calculateStatistics() {
        return CalculatorUtil.calculateMean(sum, number);
    }
}
