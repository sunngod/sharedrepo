package olek.projects.finance.cs.calc;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * INSTRUMENT2
 * Calculates mean for Nov 2014
 */
public class Instrument2Calculator implements InstrumentCalculator {
    private static final int CALC_YEAR = 2014;
    private static final int CALC_MONTH = Calendar.NOVEMBER;

    public static final String NAME = "INSTRUMENT2";

    private BigDecimal sum = BigDecimal.ZERO;
    private int number;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public synchronized void add(InstrumentData instrData) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(instrData.getDate());
        if(cal.get(Calendar.YEAR) == CALC_YEAR && cal.get(Calendar.MONTH) == CALC_MONTH) {
            sum = sum.add(instrData.getValue());
            number++;
        }
    }

    @Override
    public BigDecimal calculateStatistics() {
        return CalculatorUtil.calculateMean(sum, number);
    }
}
