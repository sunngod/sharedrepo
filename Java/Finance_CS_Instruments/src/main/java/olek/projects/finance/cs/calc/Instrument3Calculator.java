package olek.projects.finance.cs.calc;

import java.math.BigDecimal;
import java.util.Calendar;

/**
 * INSTRUMENT3
 * Calculates mean for all Mondays
 */
public class Instrument3Calculator implements InstrumentCalculator {
    private static final int CALC_DAY_OF_WEEK = Calendar.MONDAY;

    public static final String NAME = "INSTRUMENT3";

    private BigDecimal sum = BigDecimal.ZERO;
    private int number;

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public synchronized void add(InstrumentData instrData) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(instrData.getDate());

        if(cal.get(Calendar.DAY_OF_WEEK) == CALC_DAY_OF_WEEK) {
            sum = sum.add(instrData.getValue());
            number++;
        }
    }

    @Override
    public BigDecimal calculateStatistics() {
        return CalculatorUtil.calculateMean(sum, number);
    }
}
