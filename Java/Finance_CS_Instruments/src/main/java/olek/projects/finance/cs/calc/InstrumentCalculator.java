package olek.projects.finance.cs.calc;

import java.math.BigDecimal;

public interface InstrumentCalculator {

    String getName();
    void add(InstrumentData instrData);
    BigDecimal calculateStatistics();

}
