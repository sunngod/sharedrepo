package olek.projects.finance.cs.calc;

/**
 * Factory for InstrumentCalculator objects
 */
public class InstrumentCalculatorFactory {

    public InstrumentCalculator create(String instrumentName) {
        switch(instrumentName) {
            case Instrument1Calculator.NAME :
                return new Instrument1Calculator();
            case Instrument2Calculator.NAME :
                return new Instrument2Calculator();
            case Instrument3Calculator.NAME :
                return new Instrument3Calculator();
            default:
                return new InstrumentDefaultCalculator(instrumentName);
        }
    }
}
