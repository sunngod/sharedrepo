package olek.projects.finance.cs.calc;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;

/**
 * Holds Instrument data for one line of input file
 */
public class InstrumentData {
    public final static MathContext MATH_CONTEXT = MathContext.DECIMAL64;

    private String name;
    private Date date;
    private BigDecimal value;

    public InstrumentData(String name, Date date, BigDecimal value) {
        this.name = name;
        this.date = date;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Date getDate() {
        return date;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void multiplyValue(Double multiplierFactor) {
        value = value.multiply(new BigDecimal(multiplierFactor, MATH_CONTEXT));
    }
}
