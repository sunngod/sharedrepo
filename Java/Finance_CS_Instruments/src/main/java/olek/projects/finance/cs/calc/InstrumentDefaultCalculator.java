package olek.projects.finance.cs.calc;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Default Instrument calculator
 * Calculates a sum of latest 10 elements
 */
public class InstrumentDefaultCalculator implements InstrumentCalculator {
    private static final int QUEUE_MAX_SIZE = 10;
    private String name;
    private PriorityQueue<InstrumentData> queue;

    public InstrumentDefaultCalculator(String name) {
        this.name = name;
        this.queue = new PriorityQueue<InstrumentData>(QUEUE_MAX_SIZE, new InstrumentDataComparator());
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public synchronized void add(InstrumentData instrData) {
        if(queue.size() < QUEUE_MAX_SIZE) {
            queue.add(instrData);
        } else {
            if(queue.peek().getDate().before(instrData.getDate())) {
                queue.remove();
                queue.add(instrData);
            }
        }
    }

    @Override
    public BigDecimal calculateStatistics() {
        BigDecimal sum = BigDecimal.ZERO;
        for(InstrumentData data : queue) {
            sum = sum.add(data.getValue());
        }

        return sum;
    }

    private class InstrumentDataComparator implements Comparator<InstrumentData> {
        @Override
        public int compare(InstrumentData o1, InstrumentData o2) {
            return o1.getDate().compareTo(o2.getDate());
        }
    }
}
