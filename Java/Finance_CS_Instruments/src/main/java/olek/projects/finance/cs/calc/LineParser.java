package olek.projects.finance.cs.calc;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Parses one line into an instrument object
 */
public class LineParser {
    private final static Logger LOGGER = Logger.getLogger(LineParser.class.getName());
    private final static String LAST_ACCEPTABLE_DATE = "19-Dec-2014";

    private SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

    private Calendar cal = Calendar.getInstance();
    private Date lastAcceptableDate;

    public LineParser() {
        lastAcceptableDate = parseDate(LAST_ACCEPTABLE_DATE);
    }

    public InstrumentData parse(String[] nextLine) {
        if (!validateLine(nextLine)) {
            return null;
        }

        String instrName = nextLine[0];
        Date instrDate = parseDate(nextLine[1]);
        BigDecimal instrValue = parseValue(nextLine[2]);

        if(instrValue == null || instrDate == null
                || isNonBusinessDate(instrDate) || isFutureDate(instrDate)) {
            return null;
        }
        return new InstrumentData(instrName, instrDate, instrValue);
    }

    private boolean validateLine(String[] nextLine) {
        for(int i=0; i<nextLine.length; i++) {
            if(nextLine[i] == null || nextLine[i].isEmpty()) {
                LOGGER.severe("The value in column " + i + " is missing. The line is discarded");
                return false;
            }
        }
        return true;
    }

    private Date parseDate(String dateStr) {
        Date instrDate = null;
        try {
            instrDate = dateFormatter.parse(dateStr);
        } catch (ParseException | NumberFormatException ex) {
            LOGGER.log(Level.SEVERE, "Date parse error, dateStr = " + dateStr, ex);
        }

        return instrDate;
    }

    private BigDecimal parseValue(String val) {
        BigDecimal instrValue = null;
        try {
            instrValue = new BigDecimal(val);
        } catch (NumberFormatException ex) {
            LOGGER.log(Level.SEVERE, "Value parse error, value = " + val, ex);
        }

        return instrValue;
    }

    private boolean isNonBusinessDate(Date instrDate) {
        cal.setTime(instrDate);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

        if(dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
            LOGGER.fine("The date " + instrDate + " is not a business day. The line is discarded");
            return true;
        }
        return false;
    }

    private boolean isFutureDate(Date instrDate) {
        return instrDate.after(lastAcceptableDate);
    }
}
