package olek.projects.finance.cs.calc;

import olek.projects.finance.cs.db.MultiplierFactorGetter;

import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Multiplies a value of InstrumentData by DB factor
 */
public class Multiplier {
    private final static Logger LOGGER = Logger.getLogger(Multiplier.class.getName());
    private MultiplierFactorGetter multiplGetter;

    public Multiplier(MultiplierFactorGetter multiplGetter) {
        this.multiplGetter = multiplGetter;
    }

    public void multiply(InstrumentData oneLineData) {
        try {
            Double multiplierFactor = multiplGetter.get(oneLineData.getName());

            if(LOGGER.isLoggable(Level.FINEST)) {
                LOGGER.finest("multiplierFactor for " + oneLineData.getName() + " = " + multiplierFactor);
            }

            if(multiplierFactor != MultiplierFactorGetter.DEFAULT_MULTIPLIER) {
                oneLineData.multiplyValue(multiplierFactor);
            }
        } catch (ExecutionException ex) { // SQLException
            LOGGER.log(Level.SEVERE, "Problems with getting a multiplier for instrument " + oneLineData.getName(), ex);
        }
    }
}
