package olek.projects.finance.cs.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbConnection {
    public final static String DB_URL = "jdbc:derby:memory:instruments;create=true";

    private final static Logger LOGGER = Logger.getLogger(DbConnection.class.getName());

    private Connection connection;

    public void open() throws SQLException {
        LOGGER.fine("Getting connection to DB");
        connection = DriverManager.getConnection(DB_URL);
    }

    public void close() {
        try {
            if(connection != null) {
                connection.close();
            }
        } catch (SQLException ex) {
            LOGGER.log(Level.WARNING, "Error when closing Connection", ex);
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
