package olek.projects.finance.cs.db;

import java.sql.*;
import java.util.logging.Logger;

/**
 * Creates DB with INSTRUMENT_PRICE_MODIFIER in memory
 */
public class DbCreator {
    private final static Logger LOGGER = Logger.getLogger(DbCreator.class.getName());

    private Connection conn;

    public DbCreator(Connection conn) {
        this.conn = conn;
    }

    public void create() throws SQLException {
        LOGGER.info("Initialize DB in memory");

        createInstrumentsTable();
    }

    private void createInstrumentsTable() throws SQLException {
        LOGGER.fine("Creating tables");

        Statement stmt = conn.createStatement();
        try {
            stmt.executeUpdate("Create table INSTRUMENT_PRICE_MODIFIER (ID int primary key, NAME varchar(30), MULTIPLIER decimal(4,3))");
            stmt.executeUpdate("insert into INSTRUMENT_PRICE_MODIFIER values (1,'INSTRUMENT1', 1.125)");
            stmt.executeUpdate("insert into INSTRUMENT_PRICE_MODIFIER values (2,'INSTRUMENT2', 0.725)");
            stmt.executeUpdate("insert into INSTRUMENT_PRICE_MODIFIER values (3,'INSTRUMENT3', 2.0)");
        } finally {
            DbUtils.close(stmt);
        }
    }
}
