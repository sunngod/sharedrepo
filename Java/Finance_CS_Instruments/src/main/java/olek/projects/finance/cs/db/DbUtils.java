package olek.projects.finance.cs.db;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbUtils {
    private final static Logger LOGGER = Logger.getLogger(DbUtils.class.getName());

    public static void close(AutoCloseable autoCloseable) {
        try {
            if(autoCloseable != null) {
                autoCloseable.close();
            }
        } catch (Exception ex) {
            LOGGER.log(Level.WARNING, "Error when closing", ex);
        }
    }

    public static DbConnection getDbConnection() throws SQLException {
        DbConnection dbConn = new DbConnection();
        dbConn.open();
        return dbConn;
    }

    public static void shutdownDB() {
        try {
            DriverManager.getConnection("jdbc:derby:;shutdown=true");
        } catch (SQLException se) {
            if (( (se.getErrorCode() == 50000)
                    && ("XJ015".equals(se.getSQLState()) ))) {
                // expected exception
                LOGGER.info("DB shut down normally");
            } else {
                LOGGER.log(Level.SEVERE, "Derby did not shut down normally", se);
            }
        }
    }
}
