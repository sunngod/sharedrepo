package olek.projects.finance.cs.db;

import olek.projects.finance.cs.db.cache.CacheBuilder;
import olek.projects.finance.cs.db.cache.CacheLoader;
import olek.projects.finance.cs.db.cache.MultiplierFactorsCache;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

/**
 * Gets the value of Instrument's MULTIPLIER
 */
public class MultiplierFactorGetter {
    public static final Double DEFAULT_MULTIPLIER = 1.0;
    private static final Logger LOGGER = Logger.getLogger(MultiplierFactorGetter.class.getName());
    private static final int CACHE_EXPIRATION_INTERVAL_AFTER_LAST_WRITE_MS = 5;
    private static final int CACHE_EXPIRATION_INTERVAL_AFTER_LAST_CHANGE_MS = 5000;

    private Connection conn;
    private PreparedStatement prepStmt;
    private MultiplierFactorsCache multiplierFactorsCache;
    private int getFromDbCounter = 0;

    public MultiplierFactorGetter(Connection conn) throws SQLException {
        this.conn = conn;
        this.prepStmt = conn.prepareStatement(
                "SELECT MULTIPLIER FROM INSTRUMENT_PRICE_MODIFIER WHERE NAME = ?");

        multiplierFactorsCache = CacheBuilder.newBuilder()
                .maxMsToLiveAfterWrite(CACHE_EXPIRATION_INTERVAL_AFTER_LAST_WRITE_MS)
                .maxMsToLiveAfterChange(CACHE_EXPIRATION_INTERVAL_AFTER_LAST_CHANGE_MS)
                .loader(new CacheLoader() {
                    @Override
                    public Double load(String instrumentName) throws SQLException {
                        return getMultiplier(instrumentName);
                    }
                }).build();
    }

    private synchronized Double getMultiplier(String instrumentName) throws SQLException {
        getFromDbCounter++;

        prepStmt.setString(1, instrumentName);
        ResultSet rs = null;

        try {
            rs = prepStmt.executeQuery();

            if(rs.next()) {
                return rs.getDouble("MULTIPLIER");
            } else {
                return DEFAULT_MULTIPLIER;
            }
        } finally {
            DbUtils.close(rs);
        }
    }

    public synchronized Double get(String instrumentName) throws ExecutionException {
        return multiplierFactorsCache.get(instrumentName);
    }

    public synchronized void closeStatement() {
        LOGGER.fine("getFromDbCounter = " + getFromDbCounter);
        DbUtils.close(prepStmt);
    }
}
