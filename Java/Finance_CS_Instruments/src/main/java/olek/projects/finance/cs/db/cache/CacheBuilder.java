package olek.projects.finance.cs.db.cache;

/**
 * Builder for MultiplierFactorsCache
 */
public class CacheBuilder {
    private int maxMsToLiveAfterWrite;
    private int maxMsToLiveAfterChange;
    private CacheLoader cacheLoader;

    public static CacheBuilder newBuilder() {
        return new CacheBuilder();
    }

    public CacheBuilder loader(CacheLoader cacheLoader) {
        this.cacheLoader = cacheLoader;
        return this;
    }

    public CacheBuilder maxMsToLiveAfterWrite(int maxMsToLiveAfterWrite) {
        this.maxMsToLiveAfterWrite = maxMsToLiveAfterWrite;
        return this;
    }

    public CacheBuilder maxMsToLiveAfterChange(int maxMsToLiveAfterChange) {
        this.maxMsToLiveAfterChange = maxMsToLiveAfterChange;
        return this;
    }

    public MultiplierFactorsCache build() {
        return new MultiplierFactorsCache(this);
    }

    public int getMaxMsToLiveAfterWrite() {
        return maxMsToLiveAfterWrite;
    }

    public int getMaxMsToLiveAfterChange() {
        return maxMsToLiveAfterChange;
    }

    public CacheLoader getCacheLoader() {
        return cacheLoader;
    }

}
