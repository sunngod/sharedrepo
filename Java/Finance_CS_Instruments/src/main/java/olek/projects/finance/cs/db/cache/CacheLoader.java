package olek.projects.finance.cs.db.cache;

/**
 * Loads an instrument's multiplier factor from DB
 */
public interface CacheLoader {
    public Double load(String instrumentName) throws Exception;
}
