package olek.projects.finance.cs.db.cache;

/**
 * Cacheable object
 */
public interface Cacheable<E> {
    boolean isExpired();

    void setValue(E value, int maxMsToLiveAfterWrite, int maxMsToLiveAfterChange);
    E getValue();

}
