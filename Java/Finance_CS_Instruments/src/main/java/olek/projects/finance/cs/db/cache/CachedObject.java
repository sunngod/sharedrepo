package olek.projects.finance.cs.db.cache;

import java.util.Calendar;
import java.util.Date;

/**
 * An object to hold a cached value
 */
public class CachedObject<E> implements Cacheable<E> {
    private E value;
    private Date dateOfExpiration;
    private Date lastWriteTime;

    public CachedObject(E value, int maxMsToLiveAfterWrite) {
        this.value = value;
        this.lastWriteTime = new Date();
        this.dateOfExpiration = calculateExpirationDate(maxMsToLiveAfterWrite);
    }

    @Override
    public void setValue(E value, int maxMsToLiveAfterWrite, int maxMsToLiveAfterChange) {
        Date currTime = new Date();
        long liveTime = currTime.getTime() - lastWriteTime.getTime();

        if(isValueChanged(value) && (liveTime < maxMsToLiveAfterChange)) {
            int maxTimeTillNextChange = (int)(maxMsToLiveAfterChange - liveTime);
            this.dateOfExpiration = calculateExpirationDate(maxTimeTillNextChange);
        } else {
            this.dateOfExpiration = calculateExpirationDate(maxMsToLiveAfterWrite);
        }

        this.value = value;
        this.lastWriteTime = currTime;
    }

    private Date calculateExpirationDate(int maxMsToLive) {
        if(maxMsToLive <= 0) {
            return new Date();
        }

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.MILLISECOND, maxMsToLive);
        return cal.getTime();
    }

    private boolean isValueChanged(E value) {
        return this.value != null && !this.value.equals(value);
    }

    @Override
    public E getValue() {
        return value;
    }

    @Override
    public boolean isExpired() {
        return dateOfExpiration.before(new Date());
    }
}
