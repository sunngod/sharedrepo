package olek.projects.finance.cs.db.cache;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;

/**
 * Cache to hold multiplier factors
 */
public class MultiplierFactorsCache {
    private int maxMsToLiveAfterWrite;
    private int maxMsToLiveAfterChange;
    private CacheLoader cacheLoader;
    private HashMap<String, Cacheable<Double>> cache = new HashMap<>();

    public MultiplierFactorsCache(CacheBuilder cacheBuilder) {
        this.maxMsToLiveAfterWrite = cacheBuilder.getMaxMsToLiveAfterWrite();
        this.maxMsToLiveAfterChange = cacheBuilder.getMaxMsToLiveAfterChange();
        this.cacheLoader = cacheBuilder.getCacheLoader();
    }

    public Double get(String instrumentName) throws ExecutionException {
        Cacheable<Double> cachedObj = cache.get(instrumentName);

        if(cachedObj == null) {
            cachedObj = new CachedObject<>(loadFromDb(instrumentName), maxMsToLiveAfterWrite);
            cache.put(instrumentName, cachedObj);
        } else if(cachedObj.isExpired()) {
            Double newValue = loadFromDb(instrumentName);
            cachedObj.setValue(newValue, maxMsToLiveAfterWrite, maxMsToLiveAfterChange);
        }

        return cachedObj.getValue();
    }

    private Double loadFromDb(String instrumentName) throws ExecutionException {
        try {
            return this.cacheLoader.load(instrumentName);
        } catch (Exception ex) {
            throw new ExecutionException(ex);
        }
    }
}
