package olek.projects.finance.cs.calc;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DataCalculatorTest {
    @Mock
    private InstrumentCalculatorFactory calculatorFactoryMock;
    @Mock
    private InstrumentCalculator instrCalculatorMock;

    private DataCalculator dataCalculator;

    @Before
    public void setup() {
        dataCalculator = new DataCalculator() {
            @Override
            InstrumentCalculatorFactory getCalculatorFactory() {
                return calculatorFactoryMock;
            }
        };
        dataCalculator.init();
    }

    @Test
    public void shouldAddInstrumentData() {
        InstrumentData instrData = new InstrumentData("dummyInstrument", new Date(), new BigDecimal("1"));
        when(calculatorFactoryMock.create(instrData.getName())).thenReturn(instrCalculatorMock);

        dataCalculator.add(instrData);

        verify(instrCalculatorMock).add(instrData);
    }
}