package olek.projects.finance.cs.calc;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class Instrument1CalculatorTest {

    @Test
    public void shouldCalculateStatistics() {
        Instrument1Calculator instrCalc = new Instrument1Calculator();
        instrCalc.add(new InstrumentData("INSTRUMENT1", new Date(1), new BigDecimal("9.450116008")));
        instrCalc.add(new InstrumentData("INSTRUMENT1", new Date(2), new BigDecimal("118.3175")));
        instrCalc.add(new InstrumentData("INSTRUMENT1", new Date(2), new BigDecimal("83.2405")));

        BigDecimal result = instrCalc.calculateStatistics();

        assertThat("actual result = " + result, result.compareTo(new BigDecimal("70.33603866933333")), is(0));
    }
}