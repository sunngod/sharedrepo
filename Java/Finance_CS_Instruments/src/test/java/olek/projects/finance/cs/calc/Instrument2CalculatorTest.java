package olek.projects.finance.cs.calc;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class Instrument2CalculatorTest {
    @Test
    public void shouldCalculateStatistics() {
        LineParser parser = new LineParser();
        Instrument2Calculator instrCalc = new Instrument2Calculator();
        instrCalc.add(parser.parse(new String[]{"INSTRUMENT2", "5-Nov-2014", "9.450116008"}));
        instrCalc.add(parser.parse(new String[]{"INSTRUMENT2", "10-Nov-2011", "25.11"}));
        instrCalc.add(parser.parse(new String[]{"INSTRUMENT2", "10-Nov-2014", "118.3175"}));
        instrCalc.add(parser.parse(new String[]{"INSTRUMENT2", "10-Dec-2014", "88.15"}));
        instrCalc.add(parser.parse(new String[]{"INSTRUMENT2", "28-Nov-2014", "83.2405"}));

        BigDecimal result = instrCalc.calculateStatistics();

        assertThat("actual result = " + result, result.compareTo(new BigDecimal("70.33603866933333")), is(0));
    }
}