package olek.projects.finance.cs.calc;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class Instrument3CalculatorTest {
    @Test
    public void shouldCalculateStatistics() {
        LineParser parser = new LineParser();
        Instrument3Calculator instrCalc = new Instrument3Calculator();
        instrCalc.add(parser.parse(new String[]{"INSTRUMENT3", "6-Feb-2012", "9.450116008"}));
        instrCalc.add(parser.parse(new String[]{"INSTRUMENT3", "7-Feb-2012", "25.11"}));
        instrCalc.add(parser.parse(new String[]{"INSTRUMENT3", "20-Feb-2012", "118.3175"}));
        instrCalc.add(parser.parse(new String[]{"INSTRUMENT3", "22-Feb-2012", "88.15"}));
        instrCalc.add(parser.parse(new String[]{"INSTRUMENT3", "27-Feb-2012", "83.2405"}));

        BigDecimal result = instrCalc.calculateStatistics();

        assertThat("actual result = " + result, result.compareTo(new BigDecimal("70.33603866933333")), is(0));
    }
}