package olek.projects.finance.cs.calc;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class InstrumentDefaultCalculatorTest {
    @Test
    public void shouldCalculateStatistics() {
        InstrumentDefaultCalculator instrCalc = new InstrumentDefaultCalculator("dummyName");
        addTenElements(instrCalc);
        instrCalc.add(new InstrumentData("dummyName", new Date(51), new BigDecimal("51.1")));
        instrCalc.add(new InstrumentData("dummyName", new Date(52), new BigDecimal("52.2")));

        BigDecimal result = instrCalc.calculateStatistics();

        assertThat("actual result = " + result, result.compareTo(new BigDecimal("627.7")), is(0));
    }

    private void addTenElements(InstrumentDefaultCalculator instrCalc) {
        for(int i=0; i<10; i++) {
            long value = (i+1)*10;
            instrCalc.add(new InstrumentData("dummyName", new Date(value), new BigDecimal(value+"."+i)));
        }
    }
}