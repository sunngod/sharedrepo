package olek.projects.finance.cs.calc;

import org.junit.Test;

import java.util.Calendar;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class LineParserTest {
    LineParser parser = new LineParser();

    @Test
    public void shouldParseOneLine() {
        String[] oneLine = new String[]{"INSTRUMENT2","18-Nov-2002","9.775410564"};

        InstrumentData data = parser.parse(oneLine);

        assertThat(data, not(nullValue()));
        assertThat(data.getName(), is("INSTRUMENT2"));
        Calendar cal = Calendar.getInstance();
        cal.setTime(data.getDate());
        assertThat(cal.get(Calendar.YEAR), is(2002));
        assertThat(cal.get(Calendar.MONTH), is(10));
        assertThat(cal.get(Calendar.DAY_OF_MONTH), is(18));
        assertThat(data.getValue().toString(), is("9.775410564"));

//        assertThat(data.getValue().multiply(new BigDecimal("1.234567")).toString(), is("12.068399293765788"));
//        assertThat(data.getValue().multiply(new BigDecimal(1.234567, InstrumentData.MATH_CONTEXT)).toString(), is("12.068399293765788000000000"));
//        assertThat((double)(Double.parseDouble(oneLine[2])*1.234567d), is(12.068399293765786));

    }

    @Test
    public void shouldNotParseOneLineIfWrongDate() {
        String[] oneLine = new String[]{"INSTRUMENT2","wrongDate","9.775410564"};

        InstrumentData data = parser.parse(oneLine);

        assertThat(data, is(nullValue()));
    }

    @Test
    public void shouldNotParseOneLineIfWrongValue() {
        String[] oneLine = new String[]{"INSTRUMENT2","18-Nov-2002","wrongValue"};

        InstrumentData data = parser.parse(oneLine);

        assertThat(data, is(nullValue()));
    }

    @Test
    public void shouldNotParseOneLineIfEmptyName() {
        String[] oneLine = new String[]{"","18-Nov-2002","9.775410564"};

        InstrumentData data = parser.parse(oneLine);

        assertThat(data, is(nullValue()));
    }

    @Test
    public void shouldDiscardLineIfNonBusinessDay() {
        String[] oneLine = new String[]{"INSTRUMENT2","27-Sep-2014","9.77"};

        InstrumentData data = parser.parse(oneLine);

        assertThat(data, is(nullValue()));
    }

    @Test
    public void shouldParseLineIfLastAcceptableDate() {
        String[] oneLine = new String[]{"INSTRUMENT2","19-Dec-2014","9.77"};

        InstrumentData data = parser.parse(oneLine);

        assertThat(data, not(nullValue()));
    }

    @Test
    public void shouldDiscardLineIfAfterLastAcceptableDate() {
        String[] oneLine = new String[]{"INSTRUMENT2","20-Dec-2014","9.77"};

        InstrumentData data = parser.parse(oneLine);

        assertThat(data, is(nullValue()));
    }


}
