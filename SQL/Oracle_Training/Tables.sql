-- lab037
-- TMF is used - change to TRC if needed

--------------------------------------------------------
--  DDL for Table DEP
--------------------------------------------------------

  CREATE TABLE "TMF"."DEP" 
   (	"DEP_ID" NUMBER, 
	"DEP_NAME" VARCHAR2(20 BYTE), 
  "LOC_ID" NUMBER,
  "DEP_RATIO" NUMBER DEFAULT 1.0
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index DEP_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TMF"."DEP_PK" ON "TMF"."DEP" ("DEP_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table DEP
--------------------------------------------------------

  ALTER TABLE "TMF"."DEP" MODIFY ("DEP_ID" NOT NULL ENABLE);
  ALTER TABLE "TMF"."DEP" ADD CONSTRAINT "DEP_PK" PRIMARY KEY ("DEP_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;

--------------------------------------------------------
--  DDL for Table EMPL
--------------------------------------------------------

  CREATE TABLE "TMF"."EMPL" 
   (	"EMPL_ID" NUMBER, 
	"EMPL_NAME" VARCHAR2(20 BYTE), 
	"DEP_ID" NUMBER,
  "SALARY" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index EMPL_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TMF"."EMPL_PK" ON "TMF"."EMPL" ("EMPL_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table EMPL
--------------------------------------------------------

  ALTER TABLE "TMF"."EMPL" MODIFY ("EMPL_ID" NOT NULL ENABLE);
  ALTER TABLE "TMF"."EMPL" ADD CONSTRAINT "EMPL_PK" PRIMARY KEY ("EMPL_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  TABLESPACE "USERS"  ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table EMPL
--------------------------------------------------------

  ALTER TABLE "TMF"."EMPL" ADD CONSTRAINT "EMPL_DEP_FK1" FOREIGN KEY ("DEP_ID")
	  REFERENCES "TMF"."DEP" ("DEP_ID") ENABLE;

--------------------------------------------------------
--  DDL for Table LOCATION
--------------------------------------------------------

  CREATE TABLE "TMF"."LOCATION" 
   (	"LOC_ID" NUMBER, 
	"LOC_NAME" VARCHAR2(20 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  DDL for Index LOCATION_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "TMF"."LOCATION_PK" ON "TMF"."LOCATION" ("LOC_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS" ;
--------------------------------------------------------
--  Constraints for Table LOCATION
--------------------------------------------------------

  ALTER TABLE "TMF"."LOCATION" MODIFY ("LOC_ID" NOT NULL ENABLE);
  ALTER TABLE "TMF"."LOCATION" ADD CONSTRAINT "LOCATION_PK" PRIMARY KEY ("LOC_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 
  TABLESPACE "USERS"  ENABLE;

--------------------------------------------------------

INSERT INTO TMF.DEP (DEP_ID, DEP_NAME, LOC_ID) VALUES(1, 'Dep1', 1);
INSERT INTO TMF.DEP (DEP_ID, DEP_NAME, LOC_ID) VALUES(2, 'Dep2', 1);
INSERT INTO TMF.DEP (DEP_ID, DEP_NAME, LOC_ID) VALUES(3, 'Dep3', 2);
INSERT INTO TMF.DEP (DEP_ID, DEP_NAME, LOC_ID) VALUES(4, 'Dep4', null);
commit;
--DELETE from TMF.DEP where dep_id = 5;commit;

INSERT INTO TMF.EMPL (EMPL_ID, EMPL_NAME, DEP_ID, SALARY) VALUES(1, 'Empl1', 1, 100);
INSERT INTO TMF.EMPL (EMPL_ID, EMPL_NAME, DEP_ID, SALARY) VALUES(2, 'Empl2', 1, 200);
INSERT INTO TMF.EMPL (EMPL_ID, EMPL_NAME, DEP_ID, SALARY) VALUES(3, 'Empl3', 2, 100);
INSERT INTO TMF.EMPL (EMPL_ID, EMPL_NAME, DEP_ID, SALARY) VALUES(4, 'Empl4', 2, 200);
INSERT INTO TMF.EMPL (EMPL_ID, EMPL_NAME, DEP_ID, SALARY) VALUES(5, 'Empl5', 2, 400);
INSERT INTO TMF.EMPL (EMPL_ID, EMPL_NAME, DEP_ID, SALARY) VALUES(6, 'Empl6', 3, 200);
INSERT INTO TMF.EMPL (EMPL_ID, EMPL_NAME, DEP_ID, SALARY) VALUES(7, 'Empl7', 3, 400);
INSERT INTO TMF.EMPL (EMPL_ID, EMPL_NAME, DEP_ID, SALARY) VALUES(8, 'Empl8', 3, 400);
commit;

INSERT INTO TMF.LOCATION (LOC_ID, LOC_NAME) VALUES(1, 'Loc1');
INSERT INTO TMF.LOCATION (LOC_ID, LOC_NAME) VALUES(2, 'Loc2');
commit;