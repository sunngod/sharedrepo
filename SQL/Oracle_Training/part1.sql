SELECT * FROM TRC.COR_TRACE_HEADER;
SELECT * FROM TRC.COR_IMS_MSG;

SELECT id || '=' ||  parameter_name FROM TRC.cor_application_parameter;
SELECT id || q'[ '=' ]' ||  parameter_name FROM TRC.cor_application_parameter;
DESCRIBE TRC.cor_application_parameter;

SELECT id, parameter_name FROM TRC.cor_application_parameter
  WHERE parameter_name like 'ACTION%__PORT%';            -- import/export
SELECT id, parameter_name FROM TRC.cor_application_parameter
  WHERE parameter_name like 'ACTION\___PORT%' ESCAPE '\'; -- import/export
SELECT &id_column, parameter_name FROM TRC.cor_application_parameter
  WHERE parameter_name like '&param_name_pattern';
SELECT &&id_column, parameter_name FROM TRC.cor_application_parameter
  WHERE parameter_name like 'ACTION%'
  ORDER BY &id_column;

SET VERIFY ON
DEFINE param_name_pattern = 'ACTION%'
SELECT trim('A' from parameter_name) FROM TRC.cor_application_parameter
  WHERE parameter_name like '&param_name_pattern';

SELECT ROUND(45.456, 2) FROM dual;
select sysdate from dual; -- db server time
select current_date from dual; -- client timezone, not server

select (5+2) || Null from dual; -- db server time

Select INSTR('blabla', 'la', 3) from dual;