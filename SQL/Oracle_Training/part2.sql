-- Conversion: TO_CHAR (to VARCHAR2), TO_NUMUBER, TO_DATE  (xxx, [ftm], [nlsparams])

-- YYYY, YYY, YY, Y, YEAR, RR, MM, MONTH, MON, DY, DAY, DD
-- WW (week of year), W(week of month)
-- DDD (day of the year), D(day of week), DDTH, DDSP, DDSPTH
-- HH, HH12, HH24, MI, SSSSS(seconds after midnight), SS
-- AM, PM, "string"
-- /.,
-- fx: exact match of whitespaces and leading zeros, same number of digits
-- fm: removes empty characters

SELECT start_time, TO_CHAR(start_time, 'YYYY-MON-DDSPTH WW, W, DDD, DY " time:" HH24:MI AM') FROM TRC.COR_TRACE_HEADER;
select TO_CHAR(sysdate, 'fmDay, "the" Ddspth "of" Month, YYYY') from dual;
select TO_CHAR(sysdate, 'dd-mm-yyyy') from dual;
select TO_CHAR(sysdate, 'dd-mm-rr') from dual;

select TO_CHAR(1234.45, '9,999.99') from dual;
select TO_CHAR(1234.45, '9G999D99') from dual;

select TO_CHAR(TO_DATE('01-Jan-90', 'DD-MON-YY'), 'YYYY') from dual;  -- 2090
select TO_CHAR(TO_DATE('01-Jan-90', 'DD-MON-RR'), 'YYYY') from dual;  -- 1990

-- convert milliseconds to date
select to_char(to_date('1970-01-01 00','yyyy-mm-dd hh24') +
(1450448748617)/1000/60/60/24 , 'YYYY-MM-DD HH12:MI:SS am') datestr from dual;

--------------------------------------------------
--NVL, NVL2, NULLIF, COALESCE
SELECT NVL(10, 12) FROM dual; -- 10 - (second if first is null, first otherwise)
SELECT NVL(null, 12) FROM dual; -- 12
SELECT NVL2(1, 10, 12) FROM dual; -- 10
SELECT NVL2(null, 10, 12) FROM dual; -- 12
SELECT NULLIF(12, 12) FROM dual; -- null
SELECT NULLIF(12, 10) FROM dual; -- 12

SELECT COALESCE(1, 2, 3) FROM dual; -- 1 - (first non-null)
SELECT COALESCE(null, 2, 3) FROM dual; -- 2
-------------------------------------------------
SELECT id, trace_reference, start_time, 
  CASE traced_network
  WHEN 2 then 'two'
  WHEN 8 then 'eight'
  ELSE to_char(traced_network) END AS "traced_network"
FROM trc.cor_trace_header;
SELECT id, trace_reference, start_time, 
  DECODE( traced_network,
  2, 'two',
  8, 'eight',
  to_char(traced_network)) AS "traced_network"
FROM trc.cor_trace_header;

SELECT id, trace_reference, start_time, 
  CASE 
  WHEN start_time > TO_DATE('01-02-2014', 'DD-MM-YYYY') then 'New'
  ELSE 'Old' END AS "isNew"
FROM trc.cor_trace_header;
--------------------------------------------------
SELECT count(*) FROM trc.cor_trace_header; -- also null values
SELECT count(stop_time) FROM trc.cor_trace_header; -- only non-null stop_time
SELECT count(DISTINCT stop_time) FROM trc.cor_trace_header;
SELECT count(NVL(stop_time, sysdate)) FROM trc.cor_trace_header; -- equal to 'select count(*)'
SELECT count(ALL stop_time), MAX(stop_time), MIN(start_time) FROM trc.cor_trace_header;

---------------------------------------------------
-- ORDER BY, HAVING
-- if there is a GROUOP BY then any separate field in SELECT should be also in GROUP BY
-- if there is a MAX/MIN or other group function in Select then any separate field in Select is possible only if it is also in GROUP BY
-- Where is used for simple fileds, Having is used for group functions like MAX,MIN,...
SELECT count(distinct co_oc_id) from CMDCTP.ctp_common_objects;
SELECT count(ALL co_oc_id) from CMDCTP.ctp_common_objects;

SELECT co_oc_id, co_parent_gid, MAX(co_time_stamp), count(*) 
FROM CMDCTP.ctp_common_objects 
GROUP BY co_oc_id, co_parent_gid
HAVING MAX(co_time_stamp) > '01-SEP-14';

SELECT MAX(MAX(co_time_stamp))
FROM CMDCTP.ctp_common_objects 
GROUP BY co_oc_id, co_parent_gid
HAVING MAX(co_time_stamp) > '01-SEP-14';

----------------------------------------------------
-- JOINS   (NATURAL/simple INNER JOIN, LEFT/RIGHT/FULL OUTER JOIN, cross joints)

-- NATURAL JOIN: use all columns with matching names and types. It is a simplest form of inner join
-- INNER JOIN = JOIN = simple equal join, only matched rows returned
-- LEFT/RIGHT/FULL OUTER JOIN = LEFT/RIGHT/FULL JOIN = take all records from left/right table, 
--          if there are unmatched rows they are filled with nulls

-- Inner
SELECT * from TRC.EMPL NATURAL INNER JOIN TRC.DEP;
SELECT * from TRC.EMPL JOIN TRC.DEP USING (DEP_ID);
SELECT * FROM TRC.EMPL e JOIN TRC.DEP d ON e.dep_id = d.dep_id;
SELECT * FROM TRC.EMPL e, TRC.DEP d WHERE e.dep_id = d.dep_id;
SELECT * from TRC.cor_lte_trace_header a, TRC.cor_trace_ne_status b WHERE a.trace_header_id = a.trace_header_id;
-- Outer
SELECT * FROM TRC.EMPL e LEFT JOIN TRC.DEP d ON e.dep_id = d.dep_id; -- 8, no null empl, not all departments
SELECT * FROM TRC.EMPL e RIGHT JOIN TRC.DEP d ON e.dep_id = d.dep_id; -- 9, also null empl, all departments
SELECT * FROM TRC.EMPL e FULL JOIN TRC.DEP d ON e.dep_id = d.dep_id; -- 9, same results for my data
SELECT * FROM TRC.EMPL CROSS JOIN TRC.DEP; -- = SELECT * FROM TRC.EMPL e, TRC.DEP d;
----------
--Inner
SELECT * FROM TRC.DEP NATURAL JOIN TRC.LOCATION; -- 3, no null
SELECT * FROM TRC.DEP JOIN TRC.LOCATION USING(LOC_ID); -- 3, no null
SELECT * FROM TRC.DEP d, TRC.LOCATION l WHERE d.loc_id = l.loc_id; -- 3, no null
-- Outer
SELECT * FROM TRC.DEP d LEFT JOIN TRC.LOCATION l ON d.loc_id = l.loc_id; -- 4, also null location, all departments
SELECT * FROM TRC.DEP d RIGHT JOIN TRC.LOCATION l ON d.loc_id = l.loc_id; -- 3, no null location, not all all departments
----------
-- 3 tables in JOIN
SELECT * FROM TRC.EMPL e 
  JOIN TRC.DEP d USING(dep_id) 
  JOIN TRC.LOCATION USING(loc_id);
SELECT * FROM TRC.EMPL e 
  JOIN TRC.DEP d ON e.dep_id = d.dep_id 
  JOIN TRC.LOCATION l ON d.loc_id = l.loc_id
  AND l.loc_name = 'Loc1'; -- similar to Where
