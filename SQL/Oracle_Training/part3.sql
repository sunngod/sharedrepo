------------ Subquery ------------

-- Can be placed in: WHERE, HAVING, FROM
-- Can be used with operators: <>= (singlr), IN, ANY, ALL (multiple)
-- Can be single and multiple-row subquery
-- If subquery doesn't find any row it return null to the outer query

SELECT DEP_ID, MIN(SALARY) 
FROM TRC.EMPL
GROUP BY DEP_ID
HAVING MIN(SALARY) <= (SELECT MIN(SALARY)
                      FROM TRC.EMPL
                      WHERE DEP_ID = '1');
                      
-- ANY/ALL
-- < ANY means less than the maximum, > ANY means more than the minimum, =ANY is same as IN
-- < ALL means less than the minimum, > ALL means more than the maximum
SELECT * 
FROM TRC.EMPL
WHERE salary < ANY (SELECT SALARY
                      FROM TRC.EMPL
                      WHERE DEP_ID = '1');

---------------- Set operators --------------------
-- UNION (no duplications)
-- UNION ALL (with duplications, not sorted)
-- INTERSECT (rows common for both queries - all columns' values should be equal)
-- MINUS (rows in a 1st query not present in a 2nd query)
-- Column names - by 1st select, order - by 1st column bt default
-- Columns in different component queries must be in the same data group such as char or number
-- use functions/values (e.g. TO_CHAR(NULL)) to make dummy columns to match column type in another query
SELECT DEP_ID, DEP_NAME, 0 FROM TRC.DEP
UNION
SELECT DEP_ID, EMPL_NAME, SALARY FROM TRC.EMPL
ORDER BY 2;
-- is equal to
--ORDER BY DEP_NAME;


---- Exercises ------------------------------
-- 1
select employee_id, job_id from employees
UNION
select employee_id, job_id from job_history;
-- 2
select department_id from departments
MINUS
select department_id from employees where job_id = 'ST_CLERK';
-- 3
select DISTINCT job_id, department_id from employees where department_id = 10
UNION ALL
select DISTINCT job_id, department_id from employees where department_id = 50
UNION ALL
select DISTINCT job_id, department_id from employees where department_id = 20;
-- 4
select employee_id, job_id from employee
INTERSECT
select employee_id, job_id from job_history;

--------- Insert -------------------------------
-- INSERT INTO tablename (columns) VALUES (values);
-- If columns are not specified then values for ALL columns should be provided

-- copy
INSERT INTO TRC.EMPL_copy_table
SELECT * FROM TRC.EMPL;
-- can be used with column names, then column number and types should be the same in insert and subquery select statements.

--------- Update -------------------------------
-- UPDATE tablename SET column=value[, column=value, ...] [WHERE condition]

update TRC.EMPL
set (empl_name, salary) = (select empl_name, salary
  from TRC.EMPL
  where empl_id = 1)
where empl_id = 2;
commit;
-- back
update TRC.EMPL
set empl_name = 'Empl2', salary = 200
where empl_id = 2;
commit;

--------- Delete -------------------------------
-- delete [from] tablename [where condition];
-- truncate table tablename; (no rollback, no fire delete triiger - constrains to child tables should be disabled first)

--------- Transaction --------------------------
-- DML (many statements), DDL and DCL (one statement)
-- any DML statement starts a transaction
-- commit/rollback/DDL statement/exit/crash - finish transaction

update TRC.DEP SET dep_name = 'Dep1_test1' where dep_id = 1;
  savepoint mySave1;
update TRC.DEP SET dep_name = 'Dep1_test2' where dep_id = 1;
  rollback to savepoint mySave1;
commit;

-- SQL Developer: Preferences -> database -> Advanced -> Autocommit flag

-- SELECT ... FOR UPDATE [NOWAIT] - lock rows in select untill commit/rollback
SELECT e.empl_id, e.empl_name, d.dep_id, d.dep_name 
FROM TRC.EMPL e JOIN TRC.DEP d ON e.dep_id = d.dep_id
FOR UPDATE OF e.empl_id WAIT 5; -- only TRC.EMPL rows are locked. Wait 5 seconds for rows to be available for locking, if not possible after 5 seconds - exit.
rollback;

