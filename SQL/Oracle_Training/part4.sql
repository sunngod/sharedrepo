
---------- table/column name ---------

-- 1-30 characters long
-- A-Z, a-z, 0-9, _, $, #, must begin with a letter
-- no duplication for the same owner
-- if "" used when created - then they must be used always
-- names without "" are not case-sensitive, with "" - are case-sensitive
CREATE TABLE TRC.EMPL_A (	EMPL_ID NUMBER, EMPL_NAME VARCHAR2(20 BYTE));
CREATE TABLE TRC.EMPL_B [(columnNames and optionally defaultValues)] AS SELECT EMPL_ID NUMBER, EMPL_NAME FROM TRC.EMPL_A; -- no constrains passed to new table, implicit NOT NULL are not passed
  -- AS select empl_id*5 - column must have alias

-- each user owns a single schema
-- columnXX DATE DEFAULT SYSDATE
-- DDL statement is auto committed.

-- datatypes:
VARCHAR(size) -- size: 1 to 4000 
CHAR[(size)]  -- size 1(default) to 2000
NUMBER[(p,s)] 
  -- p(precision): total number of decimal digits, 1 to 38
  -- s(scale): number of digits to the right of the decimal point, -84 to 127
  NUMBER[(3,2)] = 1.23
  NUMBER[(3,-2)] = 12300
  NUMBER[(3,4)] = 0.0123
DATE -- 1.01.4712 BC to 31.12.9999 AD
TIMESTAMP -- date with fractional seconds (hundredths of a second, FF)
INTERVAL YEAR TO MONTH -- deltas
INTERVAL DAY TO SECOND -- deltas
LONG -- up to 2 GB var-length char data
  -- not copied when create table as subquery
  -- can not be in Group By or Order by
  -- can be only 1 LONG column per table
  -- no constrain for LONG column
CLOB -- up to 4 GB char data
  -- similar to LONG

RAW(size) -- row binary data, up to 2000
LONG RAW -- row binary var-length data, up to 2GB
BLOB -- binary data up to 4GB
BFILE -- binary data in external file, up to 4GB
ROWID -- used for base-64 number system representing unique address of a row in its table

---------- constrains ---------

NOT NULL
UNIQUE -- may have NULL values, null satisfy unique constraint. 
  -- In composite unique constraint 2 rows are not unique if they have partial null values and other values are identical
  -- auto-index is created
PRIMARY KEY -- it is NOT NULL
  -- auto-index is created
FOREIGN KEY
  -- values of FK must match an existing value in the parent table or be NULL
  ON DELETE CASCADE  -- delete child rows when parent row is deleted
  ON DELETE SET NULL -- set to null child value when parent row is deleted
  -- by default (if ON DELETE is not specified) child row should be deleted first
CHECK  --condition that must be true
  -- multi-check for each column is available

-- constrain can be defined during or after table creation
-- constrain can be column-level (after column definition) or table-level (in the end of create table)
-- if name not specified then SYS-Cn name is given, n is an integer
column-level: columnXX [CONSTRAINT constraintXX] constraintType
  e.g. empl_id NUMBER(6) CONSTRAINT emp_id_pk PRIMARY KEY
       dep_id NUMBER(4) CONSTRAINT emp_dep_id_fk REFERENCES departments(dep_id) -- foreign key
       salario NUMBER(4) CONSTRAINT emp_sal_chk CHECK(salario > 0) -- check
table-level: [CONSTRAINT constraintXX] constraintType(columnX, columnY)
  e.g CONSTRAINT emp_id_pk PRIMARY KEY (empl_id)
      CONSTRAINT emp_dept_fk FOREIGN KEY (dep_id) REFERENCES departments(dep_id)

----------- alter table ---------------

ALTER TABLE TRC.DEP READ ONLY;
-- perform table maintenance, no DML or SELECT FOR UPDATE is possible
ALTER TABLE TRC.DEP READ WRITE;

ALTER TABLE tableName ADD|MODIFY (columnName datatype [DEFAULT expr ] [, columnName datatype ]...);
  e.g. ALTER TABLE TRC.DEP ADD DEP_RATIO NUMBER DEFAULT 0.9;
  -- MODIFY can change type width. only type can be changed is CHAR <-> VARCHAR, only if size is not changed
ALTER TABLE tableName DROP COLUMN columnName;
ALTER TABLE tableName DROP COLUMN columnName CASCADE CONSTRAINTS; -- drop dependant constrains

ALTER TABLE tableName SET UNUSED (columnName);
  OR
ALTER TABLE tableName SET UNUSED COLUMN columnName;
  -- marks one or more columns as unused so that they can be dropped when the demand on system resources is higher
ALTER TABLE tableName DROP UNUSED COLUMNS;
  -- removes from the table all columns currently marked as unused (if any). 
ALTER TABLE tableName ADD [CONSTRAINT constrName] constrType (columnName);   -- add constraint
ALTER TABLE emp2 MODIFY empl_id PRIMARY KEY; -- add PRIMARY KEY constraint
ALTER TABLE emp2 ADD PRIMARY KEY (empl_id) USING INDEX em_id_idx2; -- add PRIMARY KEY constraint using existing index
ALTER TABLE emp2 ADD PRIMARY KEY (empl_id) USING INDEX (CREATE INDEX ...); -- add PRIMARY KEY constraint by creating named index

ALTER TABLE tableName DROP PRIMARY KEY | UNIQUE(column) | CONSTRAINT constrName [CASCADE];
  ALTER TABLE dept2 DROP PRIMARY KEY CASCADE; -- drop PK and its FKs

-- disable/enable constraint
ALTER TABLE tableName DISABLE CONSTRAINT constrName [CASCADE];
ALTER TABLE tableName ENABLE CONSTRAINT constrName;

-- constraint can be DEFERRABLE (INITIALLY DEFERRED or INITIALLY IMMEDIATE - default) or NOT DEFERRABLE
-- INITIALLY DEFERRED - to check in the end of transaction (when COMMIT)
SET CONSTRAINS ALL IMMEDIATE; --all to immediate

-- rename column/constraint
ALTER TABLE marketing RENAME COLUMN team_id TO id;
ALTER TABLE marketing RENAME CONSTRAINT mkt_pk TO mkt_pk_new;


----------- drop table ---------------

DROP TABLE table [PURGE]; -- moves the table to recycle bin
  -- data deleted, pending transactions are committed
  -- dropping table also drops its indexes and constrains, but not views and sequences
  PURGE -- to release the space to tablespace, the table doesn't go to recycle bin, FLASHBACK is not possible then
-- restore a dropped table from the recycle bin
FLASHBACK TABLE tableName TO {timestamp | SCN} expr [{ENABLE | DISABLE} TRIGGERS]
  -- SCN - a number given for a particular commit
  FLASHBACK TABLE dep2 TO BEFORE DROP;
  
select * from recyclebin;
PURGE recyclebin;

------------- view -----------------

CREATE [OR REPLACE] [FORCE] [NOFORCE] VIEW viewName [(alias1, alias2, ...)]
AS subquery [WITH CHECK OPTION [CONSTRAINT ca]] [WITH READ ONLY [CONSTRAINT cb]];
  OR REPLACE -- recreates the view
  FORCE -- regardless if base tables exist
  NOFORCE -- (default) only if base tables exist
-- if MIN,MAX, other function used in select then aliases must be specified
-- row can be deleted if no goup function, no group by, no distinct, no ROWNUM, no expression like salario*12
-- WITH CHECK OPTION is a constraint for insert/update of base tables, if update/insert have impact on the view - then error generated.
  
CREATE VIEW TRC.DEP_NAMES_VIEW
  AS Select dep_name DEPPP_NAME from TRC.DEP;
CREATE VIEW TRC.DEP_NAMES_VIEW (DEPPP_NAME)
  AS Select dep_name from TRC.DEP;
DROP VIEW TRC.DEP_NAMES_VIEW;

Select * from TRC.DEP_NAMES_VIEW;

------------- sequence -----------------

-- sequence can be shared between users
CREATE SEQUENCE TRC.DEP_SEQ MINVALUE 1 MAXVALUE 9999999 INCREMENT BY 1 START WITH 10 CACHE 20 NOORDER CYCLE;
DROP SEQUENCE TRC.DEP_SEQ;

-- usage: subquery in insert, value in insert, set in update
INSERT INTO TRC.DEP (DEP_ID, DEP_NAME, LOC_ID) VALUES(TRC.DEP_SEQ.NEXTVAL, 'Dep10', null); commit;
select * from trc.dep;
select TRC.DEP_SEQ.CURRVAL from dual;

ALTER SEQUENCE MySeq [sameParametersAsForCreate]
-- can not change START WITH
-- new MAXVALUE should be >= old MAXVALUE

------------- index -----------------
1) unique index (primary key or unique constraint, name is taken from constraint name)
2) Nonunique index (manually created)

CREATE [UNIQUE] [BITMAP] INDEX indexName ON tableName (columnName1, columnName2, ...);
DROP INDEX indexName; -- indexes can not be changed, only dropped

select * from user_indexes;

------------- synonym -----------------

-- synonym = alternative name for a table/view/sequence/procedure/otherObject
CREATE [PUBLIC] SYNONYM synonymName FOR objectName;
  PUBLIC -- means it will be accessible for all users
  -- object can not be contained in a package
DROP PUBLIC SYNONYM synonymName;

CREATE PUBLIC SYNONYM DEP_TABLE_XXX FOR TRC.DEP;
  select * from TRC.DEP;
  select * from DEP_TABLE_XXX;
DROP PUBLIC SYNONYM DEP_TABLE_XXX;




