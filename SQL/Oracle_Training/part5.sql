
----------------- privileges --------------------------------

-- security: system, database
-- privileges: 
--   1) system: to perform action on schema/s
--   2) object: action on a schema object
-- DBA generally allocate system privileges; user who owns the object can grant object privilages

-- DBA privileges
CREATE USER, DROP USER, DROP ANY TABLE, BACKUP ANY TABLE, SELECT ANY TABLE, CREATE ANY TABLE


CREATE USER userName IDENTIFIED BY pwd;
-- When CREATE USER 
   -- the user doesn't have any system privilege
   -- the user has all object privileges in his schema, can grant them to other users
ALTER USER userName IDENTIFIED BY pwdNew; -- change pwd

GRANT privilegeName [, privilegeName, ...] TO USER userName [, userName/role/PUBLIC, ...];
  PUBLIC -- every user
  -- App developer system privileges (privilegeName):
  CREATE SESSION    -- connect to DB
  CREATE TABLE, CREATE SEQUENCE, CREATE VIEW, CREATE PROCEDURE  -- in user's schema
  -- more privileges can be found here:
  select * from SESSION_PRIVS;

CREATE ROLE roleName;
  -- create user user1 identified by pwd1; 
  -- create user user2 identified by pwd2;
  -- create role manager;
  -- grant create table, create view to manager
  -- grant manager to user1, user2

GRANT {objectPrivilege|ALL} [columns] ON objectName TO userName/role/PUBLIC [WITH GRANT OPTION];
-- object privileges: ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE
-- REFERENCES - user can create foreign key to your table
-- INSERT/UPDATE/REFERENCES - can be restricted to columns
-- WITH GRANT OPTION - means that that user can futher grant it

select * from role_sys_privs;   -- sys privileges granted to role
select * from role_tab_privs;   -- table privileges granted to role
select * from user_role_privs;  -- user's roles
select * from user_sys_privs;   -- user's sys priviliges
select * from user_tab_privs_made;  -- tables priviliges granted on user's objects
select * from user_tab_privs_recd;  -- tables priviliges granted to user
select * from all_tab_privs_made;   -- grants made on user's objects

REVOKE {privilege, ... | ALL} ON objectName FROM userName, ...|roleName|PUBLIC [CASCADE CONSTRAINTS];
 -- remove privileges from userA and and cascade removing from other users granted by userA and so on 
 -- (e.g. before removing a user. If this is not done then after removing a user the privileges granted by this userA will stay)

------------ external tables --------------------------------
-- ext tables are read-only
-- DML is not allowed on ext tables
-- no indexes on ext tables

-- access drivers: ORACLE_LOADER (existing file), ORACLE_DATAPUMP (data are filled by select from another table)
CREATE OR REPLACE DIRECTORY emp_dir AS '/.../emp_dir'; -- this is needed in order to create ext table
GRANT READ ON DIRECTORY emp_dir TO hrUser;
CREATE TABLE tableName (columns) ORGANIZATION EXTERNAL (TYPE accessDriverTYpe DEFAULT DIRECTORY dirName ACCESS PARAMETERS(...) LOCATION (xxx) ) REJECT LIMIT (0|number|UNLIMITED)

------------------------------------------------------------
CREATE USER MYTESTUSER1 IDENTIFIED BY 1234;
GRANT CREATE SESSION TO MYTESTUSER1;
GRANT CREATE TABLE TO MYTESTUSER1;
grant connect, resource to MYTESTUSER1;
DROP USER MYTESTUSER1 cascade;