
----------------- data dictionary ---------------------------------------------
-- This is Oracle metadata (info about DB), read only, tables and views

-- All dictionary you have an access to
DESCRIBE DICTIONARY;
SELECT * FROM DICTIONARY;
SELECT count(*) FROM DICTIONARY;
-- search for specific dictionary
SELECT * FROM DICTIONARY where UPPER(comments) like '%AQ%';
-- see the dictionary, e.g.:
Select * from DBA_HIST_PERSISTENT_QMN_CACHE;
Select * from DBA_HIST_PERSISTENT_QUEUES;

-- Simplified dictionary info
Select * from CAT;  -- synonim for USER_CATALOG
Select * from CAT Where table_name = 'DBA_HIST_PERSISTENT_QMN_CACHE';

-- v$ - dynamic views, hold performance info

/*
 USER_OBJECTS - you owned or created in your schema
 ALL_OBJECTS  - all objects you have an access to
 DBA_OBJECTS  - all objects owned by all users

 ALL_XXX and DBA_XXX have OWNER column
*/

SELECT * FROM USER_OBJECTS;
-- OBJECT_NAME, OBJECT_ID, OBJECT_TYPE (table, view, sequence), CREATED, LAST_DDL_TIME, STATUS (valid, invalid, N/A), GENERATED (system generated/not)
SELECT * FROM ALL_OBJECTS Where UPPER(OBJECT_NAME) like '%COR_TRACE%';

-- detailed information about tables' storage
SELECT * FROM USER_TABLES; -- synonim is TABS
SELECT * FROM ALL_TABLES Where UPPER(TABLE_NAME) like '%COR_TRACE%';

-- info about columns
SELECT * FROM USER_TAB_COLUMNS;
SELECT * FROM ALL_TAB_COLUMNS;
SELECT * FROM ALL_TAB_COLUMNS Where TABLE_NAME = 'COR_TRACE_HEADER'; -- equals to DESCRIBE TRC.COR_TRACE_HEADER;
SELECT * FROM USER_UNUSED_COL_TABS; -- unused tables

-- info about constraints
SELECT * FROM USER_CONSTRAINTS;
SELECT * FROM ALL_CONSTRAINTS Where TABLE_NAME = 'COR_LTE_CALL';
-- Constraint_type:
/*   C - check constraint or table
     P - primary key
     U - unique key
     R - referrential integrity
     V - with check option
     O - with read-only, on a view
*/
-- Delete_rule:
/*  CASCADE - child records also deleted
    NO ACTION - parent can be deleted only if no children
*/
-- Status: ENABLED/DISABLED

SELECT * FROM USER_CONS_COLUMNS; -- columns used in constraints
SELECT * FROM ALL_CONS_COLUMNS Where COLUMN_NAME = 'TRACE_HEADER_ID';

-- info about indexes
SELECT * FROM USER_INDEXES;
SELECT * FROM ALL_INDEXES;
SELECT * FROM USER_IND_COLUMNS;
SELECT * FROM ALL_IND_COLUMNS WHERE TABLE_NAME = 'COR_LTE_CALL' ;

-- info about viewes
SELECT * FROM USER_VIEWS;
SELECT * FROM ALL_VIEWS;

-- info about sequences
SELECT * FROM USER_SEQUENCES;
SELECT * FROM ALL_SEQUENCES WHERE LOWER(SEQUENCE_NAME) LIKE '%cor%';

-- info about synonyms
SELECT * FROM USER_SYNONYMS;
SELECT * FROM ALL_SYNONYMS WHERE LOWER(SYNONYM_NAME) LIKE '%cor%';

-- comments
COMMENT ON TABLE TRC.COR_LTE_CALL IS 'lte calls table';
SELECT * FROM ALL_TAB_COMMENTS WHERE TABLE_NAME = 'COR_LTE_CALL';
SELECT * FROM ALL_COL_COMMENTS;

----------------- subqueries ---------------------------------------------

-- Inline view
--   WITH CHECK OPTION - checks that values pass condition of subquery (here: dep_id=2 has dep_name='Dep2')
--   similar as it is used in normal view
INSERT INTO ( SELECT EE.EMPL_ID, EE.EMPL_NAME, EE.DEP_ID, EE.SALARY 
              FROM TMF.EMPL EE
              WHERE EE.DEP_ID IN 
              (SELECT DEP_ID FROM TMF.DEP WHERE DEP_NAME = 'Dep2')
              WITH CHECK OPTION )
VALUES (9, 'Empl9', 2, 500);
commit;
-- simple insert without checking
INSERT INTO TMF.EMPL (EMPL_ID, EMPL_NAME, DEP_ID, SALARY)
VALUES (9, 'Empl9', 2, 500);
commit;

-- DEFAULT (if no default exists for this column - null value is used)
-- can be used in UPDATE and INSERT statements
UPDATE TMF.DEP
  SET DEP_RATIO = DEFAULT
  WHERE DEP_NAME='Dep2';
commit;

----------------- multitable INSERT --------------------------------------
-- unconditional INSERT (normal)
-- conditional INSERT ALL - insert into target tables only if condition is met
-- pivoting INSERT - used to make normalisation, so there are several insert into the same table clauses from select subquery, 1 record converted to several records.
-- conditional INSERT FIRST - executes only first insert clause when condition is met, skip all the rest
INSERT [ALL] [FIRST] [WHEN condition THEN]
  [insert_into_clause values_clause]
[ELSE]
  [insert_into_clause values_clause]
(subquery)
-- up to 127 when clauses
-- insert is done only for tables, not views

INSERT FIRST
  WHEN SAL > 300 THEN
    INTO TMF.EMPL_RICH VALUES(EMPID, EMPNAME, SAL)
  WHEN SAL BETWEEN 200 AND 300 THEN
    INTO TMF.EMPL_MIDDLE VALUES(EMPID, EMPNAME, SAL)
  ELSE
    INTO TMF.EMPL_POOR VALUES(EMPID, EMPNAME, SAL)
SELECT EMPL_ID EMPID, EMPL_NAME EMPNAME, SALARY SAL
FROM TMF.EMPL;
-- TMF.EMPL_RICH, TMF.EMPL_MIDDLE and TMF.EMPL_POOR should be created first

----------------- MERGE --------------------------------------------------


