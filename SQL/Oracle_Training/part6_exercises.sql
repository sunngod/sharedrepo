
---- Exercises (part1) ------------------------------
-- 1
-- You need to be able to find a list of tables that you own. Write a SQL statement that will query the data dictionary for a list of tables belonging only to you. 
SELECT * FROM ALL_TABLES Where OWNER='TRC';

-- 2
-- You want to be able to quickly view all tables that you can access. You want to view only the table's name and the table owner's name. 
-- You do not want tables that you own to appear in the list. Assuming your username is dba1
SELECT OWNER, TABLE_NAME FROM ALL_TABLES
WHERE OWNER <> 'TRC';

-- 3 
-- You need to write a script that allows users to view information for a specified table owned by that user. 
-- You want the script to prompt the user for a table name. The script should display the table's column names, data types, and data types' lengths. 
-- The precision and scale for each column should be included as well as whether nulls are allowed.
SELECT COLUMN_NAME, DATA_TYPE, DATA_LENGTH, DATA_PRECISION, DATA_SCALE FROM ALL_TAB_COLUMNS 
WHERE TABLE_NAME = '&table_name';  -- COR_TRACE_HEADER

-- 4
-- You need to create a script that reports the column name, constraint name, constraint type, search condition, and status for a specified table. 
-- You must join the USER_CONSTRAINTS and USER_CONS_COLUMNS dictionary views to obtain all this information and prompt the user to enter the table name.
DESCRIBE USER_CONSTRAINTS;
DESCRIBE USER_CONS_COLUMNS;
SELECT T1.CONSTRAINT_NAME, T1.CONSTRAINT_TYPE, T2.COLUMN_NAME, T1.SEARCH_CONDITION, T1.STATUS
FROM ALL_CONSTRAINTS T1
JOIN ALL_CONS_COLUMNS T2 ON T1.CONSTRAINT_NAME = T2.CONSTRAINT_NAME
WHERE T1.TABLE_NAME = '&table_name';

-- 5
-- Create a statement to add a comment to the DEPARTMENTS table. The comment should consist of the following text.
-- 'Company department information including name, code, and location.'
-- Then create a SQL statement that will verify that this comment has been successfully associated with the DEPARTMENTS table. 
COMMENT ON TABLE TRC.COR_LTE_CALL IS 'lte calls table';
SELECT * FROM ALL_TAB_COMMENTS WHERE TABLE_NAME = 'COR_LTE_CALL';

-- 6
-- You constantly find yourself creating queries on the EMPLOYEES table in your schema. 
-- To make writing these queries easier you want to create synonym named EMP. 
-- Provide the SQL statement required to create the synonym. Also, create a SQL statement to list the names of all synonyms in your schema. 
CREATE PUBLIC SYNONYM TRC_STATE FOR TRC.COR_APPLICATION_STATE;
  Select * From TRC_STATE;
SELECT * FROM ALL_SYNONYMS WHERE SYNONYM_NAME = 'TRC_STATE';

-- 7
-- Your schema contains multiple views. You want to view a list of these views and the SQL statements used to create them in a report.
-- SELECT OWNER, count(*) FROM ALL_VIEWS GROUP BY OWNER;
SELECT * FROM ALL_VIEWS WHERE OWNER = 'UMA';

-- 8
-- Your schema contains multiple sequences. You want to view a report that lists the following information about your sequences: sequence name, maximum value, increment size, and last number. 
DESCRIBE ALL_SEQUENCES;
SELECT SEQUENCE_NAME, MAX_VALUE, INCREMENT_BY, LAST_NUMBER FROM ALL_SEQUENCES WHERE SEQUENCE_OWNER = 'TRC';

---- Exercises (part2) ------------------------------